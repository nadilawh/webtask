<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hrefProduct</name>
   <tag></tag>
   <elementGuidId>af20b7e1-edbf-4624-913a-eeb09e0f79b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.css-uwyh54 > div.css-qa82pd > div.prd_container-card.css-126fhq2 > div.pcv3__container.css-1izdl9e > div.css-1asz3by > a.pcv3__info-content.css-gwkf0u > div.prd_link-product-name.css-3um8ox</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-testid=&quot;master-product-card&quot;])[4]//a[contains(@title, 'iPhone 15 Pro')]&#xd;
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=link[name=&quot;campaign iBox Apple iPhone 15 Pro 6.1-inch 2023 128GB 256GB 512GB Rp18.999.999 Official Store Surabaya Apple Wholesaler Rating Star 5.0 8 terjual&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3f4583f9-3d58-4883-abc5-f1640ffccc7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>prd_link-product-name css-3um8ox</value>
      <webElementGuid>b267464d-ad73-4f23-9c7b-c4ede636da9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>spnSRPProdName</value>
      <webElementGuid>871dceee-4bcb-4fc1-9848-54ce4d6cab61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>iBox Apple iPhone 15 Pro 6.1-inch 2023 128GB 256GB 512GB</value>
      <webElementGuid>bc2ed050-8e46-44b0-b2f3-c6bcfa6cdfa0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;zeus-root&quot;)/div[@class=&quot;css-8atqhb&quot;]/div[@class=&quot;css-jau1bt&quot;]/div[@class=&quot;css-1c82svt&quot;]/div[@class=&quot;css-rjanld&quot;]/div[4]/div[@class=&quot;css-jza1fo&quot;]/div[@class=&quot;css-llwpbs&quot;]/div[@class=&quot;css-54k5sq&quot;]/div[@class=&quot;css-uwyh54&quot;]/div[@class=&quot;css-qa82pd&quot;]/div[@class=&quot;prd_container-card css-126fhq2&quot;]/div[@class=&quot;pcv3__container css-1izdl9e&quot;]/div[@class=&quot;css-1asz3by&quot;]/a[@class=&quot;pcv3__info-content css-gwkf0u&quot;]/div[@class=&quot;prd_link-product-name css-3um8ox&quot;]</value>
      <webElementGuid>d7932c66-23e2-4091-a3f1-e119a7c8782e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='zeus-root']/div/div[2]/div/div[2]/div[4]/div/div/div/div/div/div/div/div[2]/a/div[2]</value>
      <webElementGuid>5f30f190-3f54-4545-a7a8-af80f4ee8166</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Beli Lokal'])[1]/following::div[1]</value>
      <webElementGuid>7e71e633-24ca-454b-9be7-8a7ed0470a6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ad'])[1]/following::div[3]</value>
      <webElementGuid>ee5f637e-89f7-40c4-a0e2-b5683a8e9199</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp18.999.999'])[1]/preceding::div[1]</value>
      <webElementGuid>a5e77697-875c-4823-acd4-28120cd70ebc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Surabaya'])[2]/preceding::div[4]</value>
      <webElementGuid>5ef29b4b-b091-48b7-bfcb-5366bf6940b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='iBox Apple iPhone 15 Pro 6.1-inch 2023 128GB 256GB 512GB']/parent::*</value>
      <webElementGuid>55dff456-46cc-4ee4-9fbd-83f1b634eef2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div/div/div/div/div[2]/a/div[2]</value>
      <webElementGuid>61b4168f-a294-467d-9a54-b41edcace5e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'iBox Apple iPhone 15 Pro 6.1-inch 2023 128GB 256GB 512GB' or . = 'iBox Apple iPhone 15 Pro 6.1-inch 2023 128GB 256GB 512GB')]</value>
      <webElementGuid>60486587-815c-4643-ba63-6d2990376119</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
