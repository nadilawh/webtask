import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://lazada.co.id')

WebUI.click(findTestObject('Lazada/formSearchLazada'))

WebUI.setText(findTestObject('Lazada/formSearchLazada'), GlobalVariable.keyword)

WebUI.click(findTestObject('Lazada/ctaCari'))

def keyword = keyword

//WebUI.comment('keyword')
//
//def text = 'Apple iPhone 15 Pro'
def text = WebUI.getText(findTestObject('Lazada/productName', [('no') : '2', ('keyword') : keyword]))

WebUI.verifyMatch(text, ".*$keyword.*", true, FailureHandling.STOP_ON_FAILURE)

// Initialize a List to store a Map for each product's details
List<Map<String, String>> allProductDetails = []

// Loop from 2 to 10 to retrieve details for each product number
for (int no = 2; no <= 5; no++) {
    Map<String, String> productDetails = [:]

    // Retrieve the product name and add it to the map with the key "productName"
    String productNameL = WebUI.getText(findTestObject('Lazada/productName', [('no') : no.toString(), ('keyword') : keyword]))
    productDetails.put('productName', productNameL)

    // Retrieve the product price and add it to the map with the key "productPrice"
    String productPrice = WebUI.getText(findTestObject('Lazada/productPrice', [('no') : no.toString(), ('keyword') : keyword]))
    productDetails.put('productPrice', productPrice)

    // Retrieve the product URL and add it to the map with the key "productUrl"
    String productUrl = WebUI.getAttribute(findTestObject('Lazada/productUrl', [('no') : no.toString(), ('keyword') : GlobalVariable.keyword]), 'href')
    productDetails.put('productUrl', productUrl)

    // Add the product details map to the list
    allProductDetails.add(productDetails)
}

//TOKOPEDIA
WebUI.openBrowser('https://tokopedia.com')

WebUI.click(findTestObject('Tokopedia/searchFormTokped'))

WebUI.setText(findTestObject('Tokopedia/searchFormTokped'), GlobalVariable.keyword)

WebUI.sendKeys(findTestObject('Tokopedia/searchFormTokped'), Keys.chord(Keys.ENTER))

//def keyword = keyword

//WebUI.comment('keyword')
//
//def text = 'Apple iPhone 15 Pro'
WebUI.delay(5)
def text2 = WebUI.getText(findTestObject('Tokopedia/productName', [('no') : '2', ('keyword') : keyword]))

WebUI.verifyMatch(text2, ".*$keyword.*", true, FailureHandling.STOP_ON_FAILURE)

//WebUI.verifyElementText(findTestObject('Lazada/productName', [('no') : '2', ('keyword') : 'iPhone 15 Pro']), keyword)

for (int no = 2; no <= 5; no++) {
	Map<String, String> productDetails = [:]

	// Retrieve the product name and add it to the map with the key "productName"
	String productName = WebUI.getText(findTestObject('Tokopedia/productName', [('no') : no.toString(), ('keyword') : keyword]))
	productDetails.put('productName', productName)

	// Retrieve the product price and add it to the map with the key "productPrice"
	String productPrice = WebUI.getText(findTestObject('Tokopedia/productPrice', [('no') : no.toString(), ('keyword') : keyword]))
	productDetails.put('productPrice', productPrice)

	// Retrieve the product URL and add it to the map with the key "productUrl"
	String productUrl = WebUI.getAttribute(findTestObject('Tokopedia/hrefProduct', [('no') : no.toString(), ('keyword') : GlobalVariable.keyword]), 'href')
	productDetails.put('productUrl', productUrl)

	// Add the product details map to the list
	allProductDetails.add(productDetails)
}
allProductDetails.sort { a, b -> a['productPrice'].compareTo(b['productPrice']) }

// Print sorted product details
allProductDetails.each { product ->
	println("")
    println("Product Name: ${product['productName']}")
    println("Product Price: ${product['productPrice']}")
    println("Product Url: ${product['productUrl']}\n")
}